#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include "Property.h"

using namespace std;

class Residential : public Property
{
public:
	Residential(string address_in, double value_in, bool isRental_in, bool isOccupied_in);
	string toString();
	string returnType();
	double computeTaxes();

protected:
	bool isOccupied;
};