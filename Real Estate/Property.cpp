#include "Property.h"

int Property::numHoldings = 0;

Property::Property(string address_in, double value_in, bool isRental_in)
{
	address = address_in;
	value = value_in;
	isRental = isRental_in;
}

string Property::getAddress()
{
	return address;
}
double Property::getValue()
{
	return value;
}
int Property::getId()
{
	return id;
}