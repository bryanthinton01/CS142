#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

#include "Residential.h"
#include "Commercial.h"

bool readFile(vector<Property*> &);
void printReport(vector<Property*>);
int boolCheck(string, int&);
double doubleCheck(string, int &);
const int NORETURN = 3;

int main()
{
	bool read;
	vector<Property*> holdings;
	read = readFile(holdings);
	if (read == true)
	{
		cout << "\n\nAll valid properties :" << endl;
		for (int i = 0; i < holdings.size(); i++)
		{
			cout << holdings[i]->toString();
		}
		printReport(holdings);
	}
	else
	{
		cout << "Invalid file name!!\n";
	}
	// system("pause");
	return 0;
}

bool readFile(vector<Property*> &holdings)
{
	string location, address, type, rental, occupied, discount, value_str, rate_str;
	double value, discountRate;
	bool isRental, hasDiscount, isOccupied;
	int fail = 0, check;
	cout << "Enter the file location: ";
	cin >> location;
	ifstream file(location);
	if (file.good())
	{
		file >> type;
		while (!file.eof())
		{
			if (type == "Residential")
			{
				file >> rental;
				check = boolCheck(rental, fail);
				if (check != NORETURN)
				{
					isRental = check;
				}

				file >> value_str;
				value = doubleCheck(value_str, fail);

				file >> occupied;
				check = boolCheck(occupied, fail);
				if (check != NORETURN)
				{
					isOccupied = check;
				}

				getline(file, address);

				if (fail > 0)
				{
					cout << "Ignoring bad RESIDENTIAL in input file: " << type << " "
						<< rental << " " << value_str << " " << occupied << " " << address << endl;
					fail = 0;
				}
				else holdings.push_back(new Residential(address, value, isRental, isOccupied));
			}
			else if (type == "Commercial")
			{
				file >> rental;
				check = boolCheck(rental, fail);
				if (check != NORETURN)
				{
					isRental = check;
				}

				file >> value_str;
				value = doubleCheck(value_str, fail);

				file >> discount;
				check = boolCheck(discount, fail);
				if (check != NORETURN)
				{
					hasDiscount = check;
				}

				file >> rate_str;
				discountRate = doubleCheck(rate_str, fail);

				getline(file, address);

				if (fail > 0)
				{
					cout << "Ignoring bad COMMERCIAL in input file: " << type << " "
						<< rental << " " << value_str << " " << discount << " " << rate_str << " "
						<< address << endl;
					fail = 0;
				}
				else holdings.push_back(new Commercial(address, value, isRental, hasDiscount, discountRate));
			}
			else
			{
				cout << "Ignoring unknown types of properties appearing in the input file: " << type << endl;
				getline(file, address);
				fail = 0;
			}
			file >> type;
			if (file.fail()) break;
		}
	}
	else return false;
	file.close();
	return true;
}

void printReport(vector<Property*> holdings)
{
	cout << "\nNOW PRINTING TAX REPORT:\n\n";
	for (int i = 0; i < holdings.size(); i++)
	{
		cout << "**Taxes due for the property at: " << holdings[i]->getAddress() << endl;
		cout << "\tProperty id:\t\t\t\t\t" << holdings[i]->getId() << endl;
		cout << "\tThis property has an estimated value of:\t" << holdings[i]->getValue() << endl;
		cout << "\tTaxes due on this property are:\t\t\t" << holdings[i]->computeTaxes() << endl << endl;
	}
}

int boolCheck(string check, int &fail)
{
	int true_false = NORETURN;

	if (check != "1" & check != "0")
	{
		fail++;
		
	}
	else if (check == "1")
	{
		true_false = 1;
	}
	else if (check == "0")
	{
		true_false = 0;
	}
	return true_false;
}

double doubleCheck(string a, int &fail)
{
	for (int i = 0; i < a.length(); i++)
	{
		if (a[i] != '0' && a[i] != '1' && a[i] != '2' && a[i] != '3' && a[i] != '4' && a[i] != '5' && a[i] != '6' && a[i] != '7' && a[i] != '8' && a[i] != '9' && a[i] != '.')
		{
			fail++;
			return -1;
		}
	}
	double output = stod(a.substr(0, a.length()).c_str());
	return output;
}