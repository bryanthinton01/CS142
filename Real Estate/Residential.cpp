#include "Residential.h"

using namespace std;

Residential::Residential(string address_in, double value_in, bool isRental_in, bool isOccupied_in) : Property(address_in, value_in, isRental_in)
{
	isOccupied = isOccupied_in;
	id = numHoldings++;
}

string Residential::toString()
{
	stringstream outString;
	string rental, occupied;
	if (isRental == 0)
		rental = "NOT rental";
	else rental = "Rental";
	if (isOccupied == 0)
		occupied = "NOT occupied";
	else occupied = "occupied";
	outString << "Property id: " << id << "  Address: " << address << "  " << rental << "  Estimated value: " << value << "  " << occupied << endl;
	return outString.str();
}

string Residential::returnType()
{
	return "Residential";
}

double Residential::computeTaxes()
{
	double tax;
	if (isOccupied == 0)
	{
		tax = .009 * value;
	}
	else tax = .006 * value;

	return tax;
}