#include "Commercial.h"

using namespace std;

Commercial::Commercial(string address_in, double value_in, bool isRental_in, bool hasDiscount_in, double discountRate_in) : Property(address_in, value_in, isRental_in)
{
	hasDiscount = hasDiscount_in;
	discountRate = discountRate_in;
	id = numHoldings++;
}

string Commercial::toString()
{
	stringstream outString;
	string rental, discount;
	if (isRental == 0)
		rental = "NOT rental";
	else rental = "Rental";
	if (hasDiscount == 0)
		discount = "NOT discounted";
	else discount = "Discounted";
	outString << "Property id: " << id << "  Address: " << address << "  " << rental << "  Estimated value: " << value << "  " << discount << "  Discount: " << discountRate << endl;
	return outString.str();
}

string Commercial::returnType()
{
	return "Commercial";
}

double Commercial::computeTaxes()
{
	double tax;
	if (isRental == 0)
	{
		tax = .01 * value;
		if (hasDiscount == 1)
		{
			tax -= tax*discountRate;
		}
	}
	else if (isRental == 1)
	{
		tax = .012 * value;
		if (hasDiscount == 1)
		{
			tax -= tax*discountRate;
		}
	}

	return tax;
}