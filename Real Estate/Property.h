#pragma once
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Property
{
public:
	Property(string address_in, double value_in, bool isRental_in);
	~Property();
	virtual string toString() = 0;
	virtual string returnType() = 0;
	virtual double computeTaxes() = 0;
	string getAddress();
	double getValue();
	int getId();

protected:
	static int numHoldings;
	int id;
	string address;
	bool isRental;
	double value;
};