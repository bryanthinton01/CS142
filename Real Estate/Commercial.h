#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include "Property.h"

using namespace std;

class Commercial : public Property
{
public:
	Commercial(string address_in, double value_in, bool isRental_in, bool hasDiscount_in, double discountRate_in);
	string toString();
	string returnType();
	double computeTaxes();

protected:
	bool hasDiscount;
	double discountRate;
};