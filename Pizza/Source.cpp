#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

/*
Guests = 1  Tip = 5%  Large = 0 Medium = 0 Small = 1 Area = 113.0974 Area/Person 113.0974 Cost $8
3			0			0		1			0			201.0619	67.0206					$11
7			10			1		0			0			314.1593	44.8799					$16
*/

int main()
{
	//
	int guests;									// Number of guests attending the event
	const int numLarge = 7;						// Guests that can be fed by a large pizza
	const int numMedium = 3;					// Guests that can be fed by a medium pizza
	int largePizza, mediumPizza, smallPizza;	// Number of pizzas that need to be purchased
	int remainder;								// Placeholder for number of people left to be fed
	const double PI = 3.14159;
	int radiusLarge = 10, radiusMedium = 8, radiusSmall = 6;	// Radius of the large, medium, and small pizzas in inches
	int percentTip;
	double priceLarge = 14.68, priceMedium = 11.48, priceSmall = 7.28;	//Price of the different Pizzas
	const int percent = 100;					// Used to convert percentTip to decimals

	cout << "How many guests are attending the event? ";
	cin >> guests;
	cout << endl;

	largePizza = guests / numLarge;
	remainder = guests % numLarge;
	mediumPizza = remainder / numMedium;
	remainder = remainder % numMedium;
	smallPizza = remainder;

	cout << "You need to order " << largePizza << " large pizzas, " << mediumPizza << " medium pizzas, and " << smallPizza << " small pizzas.\n\n";

	double surfaceArea = largePizza*PI*pow(radiusLarge, 2) + mediumPizza*PI*pow(radiusMedium, 2) + smallPizza*PI*pow(radiusSmall, 2);
	double areaPerPerson = surfaceArea / guests;

	cout << "The total surface area of the pizzas is " << surfaceArea << " square inches, or " << areaPerPerson << " square inches per person.\n\n";

	cout << "What percent tip would you like to pay? ";
	cin >> percentTip;
	cout << endl;

	double totalCost = largePizza*priceLarge + mediumPizza*priceMedium + smallPizza*priceSmall;	// Calculates the total cost of just the Pizzas										
	double costPlusTip = totalCost + percentTip * totalCost / percent;							// Adds on the price of the tip
	cout << "The price including tip will be " << fixed << setprecision(0) << "$" << costPlusTip << "\n\n";


	// system("pause");
	return(0);
}