#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <math.h>

using namespace std;
/*
Test Cases:
1	McDonald's, Wingers, Mi Ranchito, Olive Garden, Jollibee, Cafe Rio, Arby's, Mang Inasal
2	asdf	asdf has been added to index 8
3	Mi Ranchito	Mi Ranchito has been removed from index 2
4	The restaurant names have been shuffled
1	Olive Garden, Arby's, Wingers, asdf, Jollibee, Mang Inasal, Cafe Rio, McDonald's
4	1	Olive Garden, McDonald's, Arby's, Cafe Rio, Mang Inasal, asdf, Jollibee, Wingers
2	Chow King	Chow King has been added to index 8
5	You must have 2 to the n number of restaurants to begin
3	asdf	asdf has been removed from index 5
4	1	Mang Inasal, Cafe Rio, Chow King, Olive Garden, Wingers, Jollibee, McDonald's, Arby's
5	
Match 1/4, Round 1/3 --- 1: Mang Inasal or 2: Cafe Rio? 1
Match 2/4, Round 1/3 --- 1: Chow King or 2: Olive Garden? 2
Match 3/4, Round 1/3 --- 1: Wingers or 2: Jollibee? 1
Match 4/4, Round 1/3 --- 1: McDonald's or 2: Arby's? 2
Match 1/2, Round 2/3 --- 1: Mang Inasal or 2: Olive Garden? 1
Match 2/2, Round 2/3 --- 1: Wingers or 2: Arby's? 1
Match 1/1, Round 3/3 --- 1: Mang Inasal or 2: Wingers? 1
Mang Inasal is the winner!
Goodbye
*/


// Declaring functions
int menu();
void display(vector<string>&);
void addRestaurant(vector<string>&);
void removeRestaurant(vector<string>&);
int findRestaurant(vector<string>&, string);
void vectorShuffle(vector<string> &);
int checkSize(vector<string> &);
void tournament(vector<string> &);
const int NOTFOUND = -1, EXIT = 6;


int main()
{
	// Declaring variables
	int selection = 0, value = 0;
	vector<string> restaurant;
	srand(time(0));
	restaurant.push_back("McDonald's");
	restaurant.push_back("Wingers");
	restaurant.push_back("Mi Ranchito");
	restaurant.push_back("Olive Garden");
	restaurant.push_back("Jollibee");
	restaurant.push_back("Cafe Rio");
	restaurant.push_back("Arby's");
	restaurant.push_back("Mang Inasal");
	// Calls the different functions depending on what you select in the menu
	while (selection != EXIT)
	{
		selection = menu();
		switch (selection)
		{
		case 1:
			display(restaurant);
			break;
		case 2:
			addRestaurant(restaurant);
			break;
		case 3:
			removeRestaurant(restaurant);
			break;
		case 4:
			vectorShuffle(restaurant);
			break;
		case 5:
			value = checkSize(restaurant);
			if (value == NOTFOUND)
			{
				break;
			}
			tournament(restaurant);
			cout << "Goodbye\n";
			// system("pause");
			return 0;
			break;
		}
	}
	cout << "Goodbye\n";
	// system("pause");
	return 0;
}
// Gives you menu options, and checks for valid input
int menu()
{
	int selection = 0;
	while (selection == 0)
	{
		cout << "Select an option: \n1 - Display all restaurants\n2 - Add a restaurant\n3 - Remove a restaurant\n4 - Shuffle the vector\n5 - Begin the tournament\n6 - Quit the program" << endl;
		cin >> selection;
		if (cin.fail())
		{
			cin.clear();
			cin.ignore(1000, '\n');
			cout << "Invalid Input, Please try again: \n";
		}
		else if (selection < 1 || selection > EXIT)
		{
			cout << "Invalid Input, Please try again: \n";
			selection = 0;
		}
	}
	return selection;
}

// Prints the contents of the restaurant vector in order
void display(vector<string> &restaurant)
{
	for (int i = 0; i < restaurant.size(); i++)
	{
		if (i == restaurant.size() - 1)
		{
			cout << restaurant[i] << endl << endl;
		}
		else
		{
			cout << restaurant[i] << ", ";
		}
	}
}

// Adds a restaurant to the end of the restaurant vector
void addRestaurant(vector<string> &restaurant)
{
	cin.ignore(1000, '\n');
	string name;
	cout << "Enter a restaurant name: ";
	getline (cin, name);
	int i = findRestaurant(restaurant, name);
	if (i == NOTFOUND)
	{
		restaurant.push_back(name);
		cout << name << " has been added to index " << restaurant.size() - 1 << endl << endl;
	}
	else
	{
		cout << name << " can already be found at index " << i << endl << endl;
	}
	
}

// Removes a restaurant from the restaurant vector
void removeRestaurant(vector<string> &restaurant)
{
	cin.ignore(1000, '\n');
	string name;
	cout << "Enter a restaurant name to remove: ";
	getline(cin, name);
	int i = findRestaurant(restaurant, name);
	if (i == NOTFOUND)
	{
		cout << name << " was not found" << endl << endl;
	}
	else
	{
		restaurant.erase(restaurant.begin() + i);
		cout << name << " has been removed from index " << i << endl << endl;
	}

}

// Searches a vector for a particular string
int findRestaurant(vector<string> &restaurant, string name)
{
	for (int i = 0; i < restaurant.size(); i++)
	{
		if (name == restaurant[i])
		{
			return i;
		}
	}
	return NOTFOUND;
}

// Shuffles the contents of a vector
void vectorShuffle(vector<string> &restaurant)
{
	string temp;
	int seed;
	for (int i = 0; i < restaurant.size(); i++)
	{
		seed = rand() % restaurant.size();
		temp = restaurant[seed];
		restaurant[seed] = restaurant[i];
		restaurant[i] = temp;
	}
	cout << "The restaurant names have been shuffled\n\n";
}

// Makes sure a vector is to to the power of sum number
int checkSize(vector<string> &restaurant)
{
	// Here we define the max n as 10 I mean that's already a ridiculous number, I don't think we need to go higher...
	const int MAXN = 10;
	for (int n = 0; n < MAXN; n++)
	{
		if (restaurant.size() == pow(2, n))
		{
			return 0;
		}
	}
	cout << "You must have 2 to the n number of restaurants to begin\n\n";
	return NOTFOUND;
}

// Goes through each name in the vector, and lets you select which to keep
void tournament(vector<string> &restaurant)
{
	const int HALF = 2;
	int numMatches = restaurant.size() / HALF;
	int numRounds = -1, round = restaurant.size();
	int input = 0, i = 0;
	while (round > 0)
	{
		round /= HALF;
		numRounds++;
	}
	while (i < numMatches)
	{
		while (i < numMatches)
		{
			cout << "Match " << i + 1 << "/" << numMatches << ", Round " << round + 1 << "/" << numRounds << " --- 1: " << restaurant[i] << " or 2: " << restaurant[i + 1] << "? ";
			cin >> input;
			while (cin.fail() || input < 1 || input > 2)
			{
				cin.clear();
				cin.ignore(1000, '\n');
				cout << "Invalid response\n";
				cout << "Match " << i + 1 << "/" << numMatches << ", Round " << round + 1 << "/" << numRounds << " --- 1: " << restaurant[i] << " or 2: " << restaurant[i + 1] << "? ";
				cin >> input;
			}
			if (input == 1)
			{
				restaurant.erase(restaurant.begin() + i + 1);
			}
			else if (input == 2)
			{
				restaurant.erase(restaurant.begin() + i);
			}
			i++;
		}
		i = 0;
		numMatches /= HALF;
		round++;
	}
	cout << restaurant[0] << " is the winner!\n\n";
}