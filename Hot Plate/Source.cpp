#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <fstream>

using namespace std;

// Declare constants and functions
const int SIZE = 20, ON = 100, AVE = 4, SPACES = 400;
const double GAP = .1;
void initializeHotPlate(double[][SIZE]);
void printHotPlate(double[][SIZE]);
void averageHotPlate(double[][SIZE], double[][SIZE]);
void output(double[][SIZE]);

int main()
{
	// Initialize variables
	double temperature1[SIZE][SIZE]{ 0 };
	double temperature2[SIZE][SIZE]{ 0 };
	double change[SIZE][SIZE]{ 0 };
	// Initialize Hot Plate
	initializeHotPlate(temperature1);
	initializeHotPlate(temperature2);
	// Prints the 1st and 2nd iterations
	printHotPlate(temperature1);
	averageHotPlate(temperature1, temperature2);
	cout << endl << endl;
	printHotPlate(temperature2);
	int y = 0, x = 0, exit = 1, i = 0, j = 0;
	// Monitors the change of temperature between each iteration, and iterates until desired minimum change in temperature is reached 
	do
	{
			averageHotPlate(temperature2, temperature1);
			averageHotPlate(temperature1, temperature2);
			while (x >= 0 && x < SIZE)
			{
				change[x][y] = temperature2[x][y] - temperature1[x][y];
				x++;
				if (x == SIZE && y < SIZE - 1)
				{
					y++;
					x = 0;
				}
			}
			x = 0;
			y = 0;
			// Checks whether all spaces are reporting a change of less than .1 degrees.
		while (i >= 0 && i < SIZE)
		{
			if (change[i][j] < GAP)
			{
				exit++;
			}
			i++;
			if (i == SIZE && j < SIZE - 1)
			{
				j++;
				i = 0;
			}
		}
		if (exit < SPACES)
		{
			exit = 1;
			i = 0;
			j = 0;
		}
	} while (exit == 1);
	output(temperature2);
	// system("pause");
	return 0;
}

void initializeHotPlate(double temperature[][SIZE])
{
	// Initializes the upper and lower edges of a hotplate to 100
	int y = 0, x = 0;
	while (x >= 0 && x < SIZE)
	{

		if ((y == 0 || y == SIZE - 1) && (x > 0 && x < SIZE - 1))
		{
			temperature[x][y] = ON;
		}
		x++;
		if (x == SIZE && y < SIZE - 1)
		{
			y++;
			x = 0;
		}
	}
}
void printHotPlate(double temperature[][SIZE])
{
	// Prints the content of an array
	int y = 0, x = 0;
	while (x >= 0 && x < SIZE)
	{
		cout << temperature[x][y] << " ";
		x++;
		if (x == SIZE && y < SIZE - 1)
		{
			y++;
			x = 0;
			cout << endl;
		}
	}
}
void averageHotPlate(double temp1[][SIZE], double temperature2[][SIZE])
{
	// Goes through an entire array, and then outputs the average of the four values that each value is touching to a second array
	int x = 1, y = 1;
	while (y < SIZE - 1)
	{
		if (x > 0 && y > 0 && x < SIZE - 1 && y < SIZE - 1)
		{
			temperature2[x][y] = (temp1[x][y - 1] + temp1[x][y + 1] + temp1[x - 1][y] + temp1[x + 1][y]) / AVE;
		}
		y++;
		if (y == SIZE - 1 && x < SIZE - 2)
		{
			x++;
			y = 1;
		}
	}
}
void output(double temp2[][SIZE])
{
	// Outputs the final product to a .csv file.
	int x = 0, y = 0;
	ofstream fileOut("lab6output.csv");
	while (x >= 0 && x < SIZE)
	{
		fileOut << fixed << setprecision(1) << temp2[x][y] << ",";
		x++;
		if (x == SIZE && y < SIZE - 1)
		{
			y++;
			fileOut << endl;
			x = 0;
		}
	}
	fileOut.close();
}