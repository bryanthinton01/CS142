/* 
Extra Credit Answer: 166.5 MPG

Test Case Input:

1) 1000		5.00	20000	45	10000	10000	20	5000	Cost
2) 5000		4.00	40000	50	20000	20000	15	8000	Gas
3) 65412	2.34	84000	90	60000	21000	24	3000	Cost

Test Case Output:

1) 
	Non-Hybrid			Hybrid
$6250.00			$10555.00
250 gallons of gas	111 gallons of gas

2)
	Hybrid				Non-Hybrid
500 gallons of gas	1666 gallons of gas
$22000.00			$18664.00

3)
	Hybrid				Non-Hybrid
$32503.56			$49887.18
3634 gallons of gas	13627 gallons of gas

*/


#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
	double milesPerYear, gasPrice;
	double hybridCost, hybridEfficiency, hybridResale;
	double normalCost, normalEfficiency, normalResale;
	string criteria;

	// Prompts for all necessary information, and repromts if input is invalid
	cout << "What are the estimated miles driven per year? ";
	cin >> milesPerYear;
	while (milesPerYear < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> milesPerYear;
	}
	
	cout << "\nWhat is the estimated price of a gallon of gas during 5 years of ownership? ";
	cin >> gasPrice;
	while (gasPrice < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> gasPrice;
	}

	cout << "\nWhat is the initial cost of a hybrid car? ";
	cin >> hybridCost;
	while (hybridCost < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> hybridCost;
	}

	cout << "\nWhat is the efficiency of the hybrid car in miles per gallon? ";
	cin >> hybridEfficiency;
	while (hybridEfficiency < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> hybridEfficiency;
	}

	cout << "\nWhat is the estimated resale value (in dollars) for a hybrid after 5 years? ";
	cin >> hybridResale;
	while (hybridResale < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> hybridResale;
	}

	cout << "\nWhat is the initial cost of a non-hybrid car? ";
	cin >> normalCost;
	while (normalCost < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> normalCost;
	}

	cout << "\nWhat is the efficiency of the non-hybrid car in miles per gallon? ";
	cin >> normalEfficiency;
	while (normalEfficiency < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> normalEfficiency;
	}

	cout << "\nWhat is the estimated resale value (in dollars) for a non-hybrid after 5 years? ";
	cin >> normalResale;
	while (normalResale < 1)
	{
		cout << "\nInvalid input, only positive numbers are valid, please try again: ";
		cin >> normalResale;
	}

	cout << "\nEnter \"Gas\" for minimized Gas consumption, or \"Cost\" for minimized total cost: ";
	cin >> criteria;
	while (criteria != "Gas" && criteria != "Cost")
	{
		cout << "\nInvalid input, please enter either \"Gas\" or \"Cost\": ";
		cin >> criteria;
	}

	int hybridConsumption, normalConsumption;
	double normalTotalCost, hybridTotalCost;
	const int YEARS = 5;
	hybridConsumption = YEARS * milesPerYear / hybridEfficiency;
	normalConsumption = YEARS * milesPerYear / normalEfficiency;
	hybridTotalCost = gasPrice * hybridConsumption + hybridCost - hybridResale;
	normalTotalCost = gasPrice * normalConsumption + normalCost - normalResale;
	
	// Prints out the hybrid or normal specs first, depending on whether minimized Gas or Cost was selected
	cout << fixed << setprecision(2) << endl;
	if (criteria == "Cost" && hybridTotalCost > normalTotalCost)
	{
		cout << "\tNon-Hybrid \t\t\t Hybrid\n" << "$" << normalTotalCost << "\t\t\t$" << hybridTotalCost << endl;
		cout << normalConsumption << " gallons of gas\t\t" << hybridConsumption << " gallons of gas\n";
	}
	else if (criteria == "Cost" && hybridTotalCost < normalTotalCost)
	{
		cout << "\tHybrid \t\t\t Non-Hybrid\n" << "$" << hybridTotalCost << "\t\t\t$" << normalTotalCost << endl;
		cout << hybridConsumption << " gallons of gas\t\t" << normalConsumption << " gallons of gas\n";
	}
	else if (criteria == "Gas" && hybridConsumption > normalConsumption)
	{
		cout << "\tNon-Hybrid \t\t\t Hybrid\n" << normalConsumption << " gallons of gas\t\t" << hybridConsumption << " gallons of gas";
		cout << "\n$" << normalTotalCost << "\t\t\t$" << hybridTotalCost << endl;
	}
	else if (criteria == "Gas" && hybridConsumption < normalConsumption)
	{
		cout << "\tHybrid \t\t\t Non-Hybrid\n" << hybridConsumption << " gallons of gas\t\t" << normalConsumption << " gallons of gas";
		cout << "\n$" << hybridTotalCost << "\t\t\t$" << normalTotalCost << endl;
	}
	else
	{
		cout << "Ang mga kotseng ito ay pantay-pantay, sundin mo na lang ang puso mo...\n";
	}
	// system("pause");
	return(0);
}