#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

//Declare Constants
const int NUMBER_STEPS = 8, DEFAULT = 12, DEFAULT2 = 0, EXIT = 4, NUMBER_SLOTS = 9;
const double PATH_SIZE = .5;
const double PRIZE[] = { 100, 500, 1000, 0, 10000, 0, 1000, 500, 100 };
//Declare Functions
int menu();
int chipFall(double, double(&)[DEFAULT]);
void printLocation(double []);
int checkInput(int);
int checkLocation(int);
int singleChip(double(&)[DEFAULT]);
int multipleChips(double (&)[DEFAULT]);
double calculatePrize(double, int, double(&)[DEFAULT]);
int chipsAndSlots(double(&)[DEFAULT]);
int checkNegative(int);

/*Test Cases: (Because we have to)
1	4	$500.00
asdf	INVALIDdouble (&path)[DEFAULT]
2	-45	2	123456	9 INVALID
2	123456	6	$198012000.00	$1603.91
3	kwan	INVALID
3	30000	 $417043500.00

Slot 0: $785.00
Slot 1: $1021.00
Slot 2: $1599.00
Slot 3: $2288.00
Slot 4: $2533.00
Slot 5: $2277.00
Slot 6: $1594.00
Slot 7: $1020.00
Slot 8: $779.00

Everything seems to be working perfectly, yay!
*/

int main()
{
	int selection = 1;
	double path[DEFAULT];
	srand(time(0));
	//Entire program contained in a do-while loop so you can play as much as you want
	do
	{
		selection = checkInput(menu());
		switch (selection)
		{
			
		case 1:  //The program if you chose to drop only a single chip
		{
			singleChip(path);
			break;
		}
		case 2:  //The program if you chose to drop multiple chips
		{
			multipleChips(path);
			break;
		}
		case 3:  //The program if you chose to drop multiple chips in multiple slots
		{
			chipsAndSlots(path);
			break;
		}
		}
	} while (selection != EXIT); //Exits the program
	cout << "GOODBYE!\n";
	// system("pause");
	return 0;
}

int menu()
{
	// Game Menu
	int selection = 0;
	cout << "MENU: Please select one of the following options:\n\t1 - Drop a single chip into one slot\n\t2 - Drop multiple chips into one slot\n\t3 - Drop multiple chips into multiple slots\n\t4 - Quit the program\nEnter your selection now: ";
	cin >> selection;
	while (cin.fail() || selection > EXIT || selection < 1)
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "INVALID SELECTION. Please enter 1, 2, 3 or 4\n\n\n";
		cout << "MENU: Please select one of the following options:\n\t1 - Drop a single chip into one slot\n\t2 - Drop multiple chips into one slot\n\t3 - Drop multiple chips into multiple slots\n\t4 - Quit the program\nEnter your selection now: ";
		cin >> selection;
		cout << endl;
	}
	return selection;
}

int chipFall(double location, double (&path)[DEFAULT])
{
	//Calculates a radom fall path for a chip
	int evenOdd = 0;
	int count = 0;
	path[count] = location;
	while (count < DEFAULT)
	{
		evenOdd = rand();
		if (evenOdd % 2 == 0)
		{
			if (location == NUMBER_STEPS)
			{
				location -= PATH_SIZE;
			}
			else
			{
				location += PATH_SIZE;
			}
		}
		else if (evenOdd % 2 != 0)
		{
			if (location == 0)
			{
				location += PATH_SIZE;
			}
			else
			{
				location -= PATH_SIZE;
			}
		}
		count++;
		path[count] = location;
	}

	return location;
}

void printLocation(double path[])
{
	//Prints the path of a falling chip, obtained in chipFall
	int i = 0;
	cout << fixed << setprecision(1) << "\nPATH: [";
	do
	{
		if (i < DEFAULT)
		{
			cout << path[i] << " ";
		}
		else if (i == DEFAULT)
		{
			cout << path[i];
		}
		i++;
	} while (i <= DEFAULT);
	cout << "]\n";
}

int checkInput(int input)
{
	//Checks if you've entered a valid menu option
	while (cin.fail() || input < 0 || input > EXIT)
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "INVALID SELECTION.  Please enter 1, 2 or 3\n\n\n";
		input = menu();
	}
	return input;
}

int checkLocation(int input)
{
	//Checks if you've entered a valid starting location
	if (cin.fail() || input < 0 || input > NUMBER_STEPS)
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "INVALID SLOT. RETURNING TO MENU...\n\n\n";
		return 0;
	}
	return 2;
}

int singleChip(double(&path)[DEFAULT])
{
	//Performs operations for a single chip
	int endLocation = 0;
	double location = 0, totalPrize = 0;

	cout << "\n\t*** DROP SINGLE CHIP ***\nWhich slot do you want to drop the chip in (0-8)?  ";
	cin >> location;
	if (checkLocation(location) == 0)
	{
		return 0;
	}
	cout << "\n\t*** Dropping Chip into slot " << fixed << setprecision(0) << location << " ***\n";
	endLocation = chipFall(location, path);
	printLocation(path);
	cout << fixed << setprecision(2) << "WINNINGS: $" << PRIZE[endLocation] << "\n\n\n";
	return 0;
}

int multipleChips(double (&path)[DEFAULT])
{
	//Performs operations for multiple chips
	int selection = 1;
	double location = 0, chipNumber = 0, totalPrize = 0, averagePrize = 0;

	cout << "\n\t*** DROP MULTIPLE CHIPS ***\nHow many chips do you want to drop (>0)? ";
	cin >> chipNumber;
	//Checks invalid chip amounts
	if (checkNegative(chipNumber) == 1)
	{
		return 0;
	}
	cout << "Which slot do you want to drop the chip in (0-8)? ";
	cin >> location;
	selection = checkLocation(location);
	if (selection != 2)
	{
		return 0;
	}
	totalPrize = calculatePrize(location, chipNumber, path);
	//Outputs total winnings, and computes and outputs average winnings
	cout << "Total Winnings on " << fixed << setprecision(0) << chipNumber << " chips: $" << fixed << setprecision(2) << totalPrize << endl;
	averagePrize = totalPrize / chipNumber;
	cout << "Average Winnings per chip: $" << averagePrize << "\n\n\n";
	return 0;
}

double calculatePrize(double location, int chipNumber, double(&path)[DEFAULT])
{
	//Calculates total prize
	int totalPrize = 0, endLocation = 0;
	for (int i = 0; i < chipNumber; i++)
	{
		endLocation = chipFall(location, path);
		totalPrize += PRIZE[endLocation];
	}
	return totalPrize;
}

int chipsAndSlots(double(&path)[DEFAULT])
{
	double location = 0, prize = 0, totalPrize = 0, averagePrize = 0;;
	int chipNumber = 0;
	int prizePerSlot[NUMBER_SLOTS];
	cout << "\n\t*** DROP MULTIPLE CHIPS INTO EACH SLOT ***\nHow many chips do you want to drop (>0)? ";
	cin >> chipNumber;
	if (checkNegative(chipNumber) == 1)
	{
		return 0;
	}
	for (int y = 0; y < NUMBER_SLOTS; y++)
	{
		prize = calculatePrize(y, chipNumber, path);
		prizePerSlot[y] = prize;
		totalPrize += prize;
	} 
	//Outputs total winnings, and computes and outputs average winnings
	cout << "Total Winnings on " << fixed << setprecision(0) << chipNumber << " chips: $" << fixed << setprecision(2) << totalPrize << endl;
	cout << "Average Winnings per slot: \n\n";
	for (int i = 0; i < NUMBER_SLOTS; i++)
	{
		averagePrize = prizePerSlot[i] / chipNumber;
		cout << "Slot " << i << ": $" << averagePrize << endl;
	}
	cout << endl << endl;
	return 0;
}

int checkNegative(int input)
{
	//Checks if the input is negative
	if (cin.fail() || input < 1)
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "INVALID NUMBER OF CHIPS. RETURNING TO MENU...\n\n\n";
		return 1;
	}
	return 0;
}