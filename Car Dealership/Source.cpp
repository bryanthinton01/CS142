#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <fstream>
#include "Car.h"

/* Test Cases
3	Tesla	Red	80000	You can't afford this car!
3	Corsica	Burgundy	1150	Car purchased
1	Corsica	Burgundy	$1150
4	dfdsf	Car not owned
3	Camry	Gray	8000	Car purchased
5	Camry	Blue	Car painted blue
2	$850.00
6	cars2.txt	File loaded
1	Corsica	Burgundy	$1150
	Camry	Blue	$10000
	Navy	White	$6972.15 etc.
2	$60565.43
7	Kwan.txt	File Saved
8	Goodbye
*/

using namespace std;

// Declaring functions
int checkName(vector<Car*>, string);
void buyCar(vector<Car*>&, double &);
void sellCar(vector<Car*>&, double &);
void paintCar(vector<Car*>&);
void loadFile(vector<Car*>&, double &);
void saveFile(vector<Car*>, double);
const int NOTFOUND = -1;

int main()
{
	// Declaring variables
	const int EXIT = 8;
	double balance = 10000, price = 0;
	int input = 0;
	vector<Car*> inventory;
	while (input != EXIT) // Menu
	{
		cout << "Please select an option:\n1 - Show Current Inventory\n2 - Show Current Balance\n3 - Buy a Car\n"
			<< "4 - Sell a Car\n5 - Paint a Car\n6 - Load File\n7 - Save File\n8 - Quit Program\n\n";
		cin >> input;
		if (input > EXIT || input < 1 || cin.fail())
		{
			cin.clear();
			cin.ignore(1000, '\n');
			cout << "INVALID INPUT\n\n";
		}
		else
		{
			switch (input) 
			{
			case 1:
			{
				if (inventory.size() == 0)
				{
					cout << "Inventory Empty\n\n";
				}
				else
				{
					for (int i = 0; i < inventory.size(); i++)
					{
						cout << inventory[i]->toString() << endl;
					}
				}
				break;
			}
			case 2:
				cout << "Your balance is $" << fixed << setprecision(2) << balance << endl << endl;
				break;
			case 3:
				buyCar(inventory, balance);
				break;
			case 4:
				sellCar(inventory, balance);
				break;
			case 5:
				paintCar(inventory);
				break;
			case 6:
				loadFile(inventory, balance);
				break;
			case 7:
				saveFile(inventory, balance);
				break;
			}
		}
	}
	cout << "Goodbye\n";
	// system("pause");
	return 0;
}

// Checks whether a car name is in the inventory
int checkName(vector<Car*>inventory, string name)
{
	for (int i = 0; i < inventory.size(); i++)
	{
		if (name == inventory[i]->getName())
		{
			return i;
		}
	}
	return NOTFOUND;
}

// Adds a car to the inventory
void buyCar(vector<Car*>&inventory, double &balance)
{
	int check = 0;
	string name, color;
	double price;
	cout << "Name: ";
	cin >> name;
	cout << "Color: ";
	cin >> color;
	cout << "Price: ";
	cin >> price;
	check = checkName(inventory, name);
	if (check == NOTFOUND)
	{
		if (price > balance)
		{
			cout << "You can't afford this car!\n\n";
		}
		else
		{
			inventory.push_back(new Car(name, color, price));
			cout << "Car Purchased\n\n";
			balance -= price;
		}
	}
	else
	{
		cout << "You already own this car!\n\n";
	}
}

// Removes a car from the inventory
void sellCar(vector<Car*>&inventory, double &balance)
{
	string name;
	int check = 0;
	cout << "Car to sell: ";
	cin >> name;
	check = checkName(inventory, name);
	if (check == NOTFOUND)
	{
		cout << "Car not owned\n\n";
	}
	else
	{
		balance += inventory[check]->getPrice();
		inventory.erase(inventory.begin() + check);
		cout << "Car sold\n\n";
	}
}

// Changes the color of a car object
void paintCar(vector<Car*>&inventory)
{
	string name, color;
	int check;
	cout << "Car name: ";
	cin >> name;
	check = checkName(inventory, name);
	if (check == NOTFOUND)
	{
		cout << "Car not owned\n\n";
	}
	else
	{
		cout << "New color: ";
		cin >> color;
		inventory[check]->paint(color);
		cout << "Car painted " << color << "\n\n";
	}
}

// Adds the contents of a specially formatted file to the program
void loadFile(vector<Car*>&inventory, double &balance)
{
	string name, color, filename;
	double price, newBalance;
	cout << "Enter file name: ";
	cin >> filename;
	ifstream file(filename);
	file >> newBalance;
	balance += newBalance;
	while (!file.eof())
	{
		file >> name >> color >> price;
		inventory.push_back(new Car(name, color, price));
	}
	file.close();
	cout << "File loaded\n\n";
}

// Saves the inventory and balance to a file for later use
void saveFile(vector<Car*>inventory, double balance)
{
	string filename;
	cout << "Enter file name: ";
	cin >> filename;
	ofstream  file(filename);
	file << fixed << setprecision(2) << balance << endl;
	for (int i = 0; i < inventory.size(); i++)
	{
		if (i == inventory.size() - 1)
		{
			file << inventory[i]->getName() << " " << inventory[i]->getColor() << " " << inventory[i]->getPrice();
		}
		else
		{
			file << inventory[i]->getName() << " " << inventory[i]->getColor() << " " << inventory[i]->getPrice() << endl;
		}
	}
	file.close();
	cout << "File saved\n\n";
}